package br.com.mario.batch.primeirorefatorado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrimeirorefatoradoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrimeirorefatoradoApplication.class, args);
	}

}
