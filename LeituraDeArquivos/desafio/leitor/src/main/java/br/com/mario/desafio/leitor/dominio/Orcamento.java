package br.com.mario.desafio.leitor.dominio;

import java.util.ArrayList;
import java.util.List;

public class Orcamento {

    private String codigo;
    private String descricaoNatureza;
    private double total;
    private List<ItemOrcamento> itemOrcamentos = new ArrayList<>();

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricaoNatureza() {
        return descricaoNatureza;
    }

    public void setDescricaoNatureza(String descricaoNatureza) {
        this.descricaoNatureza = descricaoNatureza;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public List<ItemOrcamento> getItemOrcamentos() {
        return itemOrcamentos;
    }

    public void setItemOrcamentos(List<ItemOrcamento> itemOrcamentos) {
        this.itemOrcamentos = itemOrcamentos;
    }

    @Override
    public String toString() {
        return "Orcamento{" +
                "codigo='" + codigo + '\'' +
                ", descricaoNatureza='" + descricaoNatureza + '\'' +
                ", total=" + total +
                ", itemOrcamentos=" + itemOrcamentos +
                '}';
    }
}
