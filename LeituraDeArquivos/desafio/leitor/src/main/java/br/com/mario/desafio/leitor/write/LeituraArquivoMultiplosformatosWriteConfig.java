package br.com.mario.desafio.leitor.write;

import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LeituraArquivoMultiplosformatosWriteConfig {

    @Bean
    public ItemWriter leituraArquivoDelimitadoWriter() {
        return items -> items.forEach(System.out::println);
    }
}
