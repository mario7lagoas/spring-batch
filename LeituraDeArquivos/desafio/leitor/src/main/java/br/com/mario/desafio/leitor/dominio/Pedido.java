package br.com.mario.desafio.leitor.dominio;

public class Pedido {
    private String codigo;
    private String descricaoNatureza;
    private String item;
    private String data;
    private double valor;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricaoNatureza() {
        return descricaoNatureza;
    }

    public void setDescricaoNatureza(String descricaoNatureza) {
        this.descricaoNatureza = descricaoNatureza;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Pedido{" +
                "codigo='" + codigo + '\'' +
                ", descricaoNatureza='" + descricaoNatureza + '\'' +
                ", item='" + item + '\'' +
                ", data='" + data + '\'' +
                ", valor=" + valor +
                '}';
    }
}
