package br.com.mario.desafio.leitor.reader;


import br.com.mario.desafio.leitor.dominio.ItemOrcamento;
import br.com.mario.desafio.leitor.dominio.Orcamento;
import br.com.mario.desafio.leitor.dominio.Pedido;
import org.springframework.batch.item.*;

import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.ResourceAwareItemReaderItemStream;
import org.springframework.core.io.Resource;

import java.util.ArrayList;
import java.util.List;

public class ArquivoDemonstrativoOrcamentoReader implements ItemStreamReader<Orcamento>,
        ResourceAwareItemReaderItemStream<Orcamento> {

    private Object objAtual;
    private FlatFileItemReader<Object> delegate;
    private String numeroOrcamento ;

    private double totalizador = 0;

    public ArquivoDemonstrativoOrcamentoReader(FlatFileItemReader<Object> delegate){
        this.delegate = delegate;
    }

    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {
        delegate.open(executionContext);
    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {
     delegate.update(executionContext);
    }

    @Override
    public void close() {
        delegate.close();
    }


    @Override
    public void setResource(Resource resource) {
        delegate.setResource(resource);
    }

    @Override
    public Orcamento read() throws Exception{
        if (objAtual == null)
            objAtual = delegate.read();


      /*  String codigoSwap = numeroOrcamento;
       Orcamento orcamento = new Orcamento();
        numeroOrcamento = orcamento.getCodigo();
        objAtual = null;
        */


        Pedido pedido = (Pedido) objAtual;
        objAtual = null;





        Orcamento orcamento = new Orcamento();





        if (pedido != null) {
            if (!numeroOrcamento.equals(pedido.getCodigo()) ){
                totalizador = 0;

            }else{
                totalizador += pedido.getValor();
            }
            numeroOrcamento = pedido.getCodigo();


            orcamento.setCodigo(pedido.getCodigo());
            orcamento.setDescricaoNatureza(pedido.getDescricaoNatureza());
          //  orcamento.setTotal(totalizador);
           /*
            List<ItemOrcamento> itemOrcamentos = new ArrayList<>();

            ItemOrcamento itemOrcamento = new ItemOrcamento();
            itemOrcamento.setData(pedido.getData());
            itemOrcamento.setItem(pedido.getItem());
            itemOrcamento.setValor(pedido.getValor());

            itemOrcamentos.add(itemOrcamento);
            if (!itemOrcamentos.isEmpty())
                orcamento.setItemOrcamentos(itemOrcamentos);

            */
        }
       /*
        if (codigoSwap.equals(numeroOrcamento)){
            while (peek() instanceof ItemOrcamento )
                orcamento.getItemOrcamentos().add((ItemOrcamento) objAtual);
        }
        */

      /*
        if (orcamento != null){
            while (peek() instanceof )
        }

       */

        return orcamento;
    }

    private Object peek() throws Exception{
        objAtual = delegate.read();
        return objAtual;
    }
}
