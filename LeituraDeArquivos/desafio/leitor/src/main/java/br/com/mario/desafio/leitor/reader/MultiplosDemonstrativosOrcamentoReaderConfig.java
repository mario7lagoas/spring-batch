package br.com.mario.desafio.leitor.reader;

import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.builder.MultiResourceItemReaderBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Configuration
public class MultiplosDemonstrativosOrcamentoReaderConfig {

    @Bean
    @StepScope
    public MultiResourceItemReader multiplosDemonstrativosOrcamentoReader(
            @Value("#{jobParameters['arquivosDemonstrativos']}") Resource[] arquivosDemonstrativo,
            FlatFileItemReader leituraArquivoMultiploFormatosReader
    ){
        return new MultiResourceItemReaderBuilder<>()
                .name("multiplosDemonstrativosOrcamentoReader")
                .resources(arquivosDemonstrativo)
                .delegate( new ArquivoDemonstrativoOrcamentoReader(leituraArquivoMultiploFormatosReader))
                .build();

    }
}


