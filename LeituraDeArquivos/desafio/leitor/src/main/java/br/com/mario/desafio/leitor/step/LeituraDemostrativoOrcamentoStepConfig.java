package br.com.mario.desafio.leitor.step;

import br.com.mario.desafio.leitor.dominio.Orcamento;
import br.com.mario.desafio.leitor.dominio.Pedido;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LeituraDemostrativoOrcamentoStepConfig {

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public Step leituraDemostrativoOrcamentoStep(
            MultiResourceItemReader<Pedido> multiplosDemonstrativosOrcamentoReader,
            ItemWriter leituraArquivoMultiplosDemonstrativoOrcaomentoItemWrite
    ){
        return stepBuilderFactory
                .get("leituraDemostrativoOrcamentoStep")
                .chunk(1)
                .reader(multiplosDemonstrativosOrcamentoReader)
                .writer(leituraArquivoMultiplosDemonstrativoOrcaomentoItemWrite)
                .build();
    }
}
