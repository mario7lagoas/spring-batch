package br.com.mario.desafio.leitor.reader;

import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Configuration
public class ArquivoMultiplosFormatosReaderConfig {
    @Bean
    @StepScope
    public FlatFileItemReader arquivoMultiplosFormatosItemReader(
            @Value("#{jobParameters['arquivosDemonstrativos']}") Resource arquivosDemonstrativos,
            LineMapper lineMapper
    ){
        return new FlatFileItemReaderBuilder<>()
                .name("arquivoMultiplosFormatosItemReader")
                .resource(arquivosDemonstrativos)
                .lineMapper(lineMapper)
                .build();
    }
}
