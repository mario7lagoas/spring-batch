package br.com.mario.desafio.leitor.dominio;

public class ItemOrcamento {
    private String data;
    private String item;
    private double valor;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "ItemOrcamento{" +
                "data='" + data + '\'' +
                ", item='" + item + '\'' +
                ", valor=" + valor +
                '}';
    }
}
