package com.springbatch.arquivomultiplosformatos.reader.linemapper;

import com.springbatch.arquivomultiplosformatos.dominio.Cliente;
import com.springbatch.arquivomultiplosformatos.dominio.Transacao;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.mapping.PatternMatchingCompositeLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class ClienteTransacaoLineMapperConfig {

    @Bean
    public PatternMatchingCompositeLineMapper lineMapper (){
        PatternMatchingCompositeLineMapper lineMapper = new PatternMatchingCompositeLineMapper();

        lineMapper.setTokenizers(tokenizer());
        lineMapper.setFieldSetMappers(fieldSetMappers());

        return lineMapper;
    }

    private Map<String, FieldSetMapper> fieldSetMappers() {
        Map<String,FieldSetMapper>  fieldSetMapperMap = new HashMap<>();
        fieldSetMapperMap.put("0*", fieldSetMapperMap(Cliente.class));
        fieldSetMapperMap.put("1*", fieldSetMapperMap(Transacao.class));

        return fieldSetMapperMap;
    }

    private FieldSetMapper fieldSetMapperMap(Class classe) {
        BeanWrapperFieldSetMapper fieldSetMapper = new BeanWrapperFieldSetMapper();
        fieldSetMapper.setTargetType(classe);

        return fieldSetMapper;
  }

    private Map<String, LineTokenizer> tokenizer() {
        Map<String, LineTokenizer>  tokenizerMap = new HashMap<>();
        tokenizerMap.put("0*", clienteLineTokennizer());
        tokenizerMap.put("1*", transacaoLineTokennizer());

        return tokenizerMap;
    }

    private LineTokenizer transacaoLineTokennizer() {
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames("id", "descricao", "valor");
        lineTokenizer.setIncludedFields(1, 2 , 3);

        return lineTokenizer;

    }

    private LineTokenizer clienteLineTokennizer() {
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames("nome", "sobrenome", "idade", "email");
        lineTokenizer.setIncludedFields(1, 2 , 3, 4);

        return lineTokenizer;

    }

}