package com.springbatch.contasbancarias.writer;

import com.springbatch.contasbancarias.dominio.Conta;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

@Configuration
public class FileContaWriteConfig {

    @Bean
    public FlatFileItemWriter<Conta> fileContaWrite(){

        return new FlatFileItemWriterBuilder<Conta>()
                .name("fileContaWrite")
                .resource(new FileSystemResource("./files/Contas.txt"))
                .delimited()
                .names("tipo", "limite", "clienteId")
                .build();

    }

}
