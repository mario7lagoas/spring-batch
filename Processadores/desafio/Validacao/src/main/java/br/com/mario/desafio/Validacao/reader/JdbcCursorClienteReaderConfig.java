package br.com.mario.desafio.Validacao.reader;

import br.com.mario.desafio.Validacao.dominio.Cliente;

import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import javax.sql.DataSource;

@Configuration
public class JdbcCursorClienteReaderConfig {

    @Bean
    public JdbcCursorItemReader<Cliente> jdbcCursorClienteReader(@Qualifier("xptoDataSource") DataSource dataSource){

        return new JdbcCursorItemReaderBuilder<Cliente>()
                .name("jdbcCursorClienteReader")
                .dataSource(dataSource)
                .sql("select * from cliente")
                .rowMapper(new BeanPropertyRowMapper<Cliente>(Cliente.class))
                .build();

    }
}
