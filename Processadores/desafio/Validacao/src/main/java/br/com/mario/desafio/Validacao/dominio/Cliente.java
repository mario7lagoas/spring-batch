package br.com.mario.desafio.Validacao.dominio;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Cliente {

    @NotNull
    @Size(min = 1, max = 100)
    @NotNull
    private String nome;
    @NotNull
    private  int idade;
    @NotNull
    @Email
    private String email;
    @NotNull
    private Double faixaSalarial;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getFaixaSalarial() {
        return faixaSalarial;
    }

    public void setFaixaSalarial(double faixaSalarial) {
        this.faixaSalarial = faixaSalarial;
    }
}
