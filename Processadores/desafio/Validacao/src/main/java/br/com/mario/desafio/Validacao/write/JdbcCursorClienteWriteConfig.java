package br.com.mario.desafio.Validacao.write;

import br.com.mario.desafio.Validacao.dominio.Cliente;
import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JdbcCursorClienteWriteConfig {

    @Bean
    public ItemWriter<Cliente> jdbcCursorClienteWriter() {
        return clientes -> clientes.forEach(cliente -> {
            System.out.println("Salario : " + cliente.getFaixaSalarial() +
                    " - Email : " + cliente.getEmail() +
                    " - Nome  : " + cliente.getNome());
        });
    }
}
