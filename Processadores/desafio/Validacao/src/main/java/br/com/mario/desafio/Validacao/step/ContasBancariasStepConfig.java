package br.com.mario.desafio.Validacao.step;

import br.com.mario.desafio.Validacao.dominio.Cliente;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ContasBancariasStepConfig {

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public Step contasBancariaStep(ItemReader<Cliente> jdbcCursorReader,
                                   ItemWriter<Cliente> jdbcCursorWriter,
                                   ItemProcessor<Cliente, Cliente> processadorValidacaoProcessor){
        return stepBuilderFactory
                .get("contasBancariaStep")
                .<Cliente,Cliente>chunk(1)
                .reader(jdbcCursorReader)
                .writer(jdbcCursorWriter)
                .processor(processadorValidacaoProcessor)
                .build();
    }
}
