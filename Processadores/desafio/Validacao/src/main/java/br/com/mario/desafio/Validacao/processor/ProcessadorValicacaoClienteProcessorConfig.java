package br.com.mario.desafio.Validacao.processor;

import br.com.mario.desafio.Validacao.dominio.Cliente;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.validator.BeanValidatingItemProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProcessadorValicacaoClienteProcessorConfig {

    @Bean
    public ItemProcessor<Cliente, Cliente> processadorValicacaoClienteProcessor(){

        BeanValidatingItemProcessor<Cliente> processor = new BeanValidatingItemProcessor<>();
        processor.setFilter(true);

        return processor;
    }
}
